package com.example.viewmodeldemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.fragment.app.viewModels
const val TAG = "Demo ViewModel"
class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var mCounterViewModel: CounterViewModel

    private lateinit var btnUp: ImageButton
    private lateinit var btnDown: ImageButton
    private lateinit var tvCount: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mCounterViewModel = ViewModelProvider(this)[CounterViewModel::class.java]

        btnUp = findViewById(R.id.btnUp)
        btnDown = findViewById(R.id.btnDown)
        tvCount = findViewById(R.id.tvCount)

        displayCount()

        btnUp.setOnClickListener(this)
        btnDown.setOnClickListener(this)
    }

    private fun displayCount() {
        tvCount.text = mCounterViewModel?.count.toString()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            btnUp.id -> {
                mCounterViewModel?.count = mCounterViewModel?.count!! + 1
                displayCount()
            }

            btnDown.id -> {
                mCounterViewModel?.count = mCounterViewModel?.count!! - 1
                displayCount()
            }
        }
    }
}